import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import vueHeadful from 'vue-headful'
import { firebase } from '@firebase/app'
import VueConfetti from 'vue-confetti'
import '@firebase/firestore'
import '@firebase/auth'

Vue.use(require('vue-moment'))
Vue.use(VueConfetti)

Vue.use(Buefy, {
  defaultIconPack: 'fad',
})
Vue.component('vue-headful', vueHeadful)
Vue.config.productionTip = false

const firebaseConfig = {
  apiKey: "AIzaSyCFjU1SjmCBTeX6Afe_BbaKovRNyD1XJNo",
  authDomain: "dencap-covid.firebaseapp.com",
  databaseURL: "https://dencap-covid.firebaseio.com",
  projectId: "dencap-covid",
  storageBucket: "dencap-covid.appspot.com",
  messagingSenderId: "549274490039",
  appId: "1:549274490039:web:b6e826e8d5da8430cf6932",
  measurementId: "G-NKG3DYJYDB"
}
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export const db = firebase.firestore()

let app = ''

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
