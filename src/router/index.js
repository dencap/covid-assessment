import Vue from "vue";
import Router from "vue-router";
import firebase from "@firebase/app";
import "@firebase/auth";
import store from "../store";

Vue.use(Router);

let admins = [
  "spalmer@hmidental.com",
  "ddorsch@dencap.com",
  "pstigall@hmidental.com",
  "kmott@dencap.com",
  "tbailey@dencap.com",
  "tbailey@hmidental.com",
  "ekelley@dencap.com",
  "jtlentine@dencap.com",
];

const router = new Router({
  mode: "history",
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/login",
      name: "Login",
      component: () => import("@/views/Login.vue"),
    },
    {
      path: "/assessment",
      name: "Assessment",
      component: () => import("@/views/Assessment.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/admin",
      name: "Admin",
      component: () => import("@/views/Admin.vue"),
      beforeEnter: (to, from, next) => {
        const authEmail = store.state.userAuthData.email;
        if (admins.includes(authEmail)) next();
        else next("/assessment");
      },
    },
    {
      path: "*",
      redirect: "/login",
    },
  ],
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((x) => x.meta.requiresAuth);
  const currentUser = firebase.auth().currentUser;
  if (requiresAuth && !currentUser) {
    next({
      path: "/login",
      query: { redirect: to.fullPath },
    });
  } else if (requiresAuth && currentUser) {
    next();
  } else {
    next();
  }
});

export default router;
