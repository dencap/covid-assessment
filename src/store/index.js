import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: "vuex", // The key to store the state on in the storage provider.
  storage: window.sessionStorage, // or window.sessionStorage or localForage
});

const getDefaultState = () => {
  return {
    userAuthData: [],
    currentStep: 1,
    responses: {
      acknowledgement: undefined,
      majorSymptoms: [],
      minorSymptoms: [],
      temperature: undefined,
      closeContact: undefined,
    },
    results: {
      riskRating: 0,
      reason: "",
    },
  };
};

export default new Vuex.Store({
  state: getDefaultState(),
  getters: {
    userAuthData: (state) => state.userAuthData,
    responses: (state) => state.responses,
    progress: (state) => state.progress,
    currentStep: (state) => state.currentStep,
  },
  mutations: {
    nextStep(state) {
      state.currentStep++;
    },
    setUserAuthData(state, payload) {
      state.userAuthData = payload;
    },
    setAcknowledgement(state, payload) {
      state.responses.acknowledgement = payload;
    },
    setTemperature(state, payload) {
      state.responses.temperature = payload;
    },
    setMajorSymptoms(state, payload) {
      state.responses.majorSymptoms = payload;
    },
    setMinorSymptoms(state, payload) {
      state.responses.minorSymptoms = payload;
    },
    setCloseContact(state, payload) {
      state.responses.closeContact = payload;
    },
    resetAssessment(state) {
      Object.assign(state, getDefaultState());
    },
  },
  modules: {},
  plugins: [vuexLocalStorage.plugin],
});
